<?php

/**
 * @file
 * All the Linktags to HTML conversions are handled in this file.
 */

function _linktags_filter_process(&$body, $settings) {
  // Encode all script tags to prevent XSS html injection attacks
  $body = preg_replace(array('#<script([^>]*)>#i', '#</script([^>]*)>#i'), array('&lt;script\\1&gt;', '&lt;/script\\1&gt;'), $body);

  $eml_tag = '<span class="lt-eml">@:</span> ';
  $tel_tag = '<span class="lt-tel">t:</span> ';
  $doc_tag = '<span class="lt-doc">i:</span> ';
  $int_tag = '<span class="lt-int">i:</span> ';
  $ext_tag = '<span class="lt-ext">e:</span> ';
  $efm_tag = '<span class="lt-eform">e:</span> ';

  if ($settings['linktags_make_title']) {
  	$title_attr_2=' title="\\2"';
  } else {
  	$title_attr_2='';
  }
  
  // Define Linktags tags
  $preg = array(

    // Email @: link, with < > and without extra text: [email]name <local@addr>[/email]
    '#\[email\]([[:alnum:]&\-+,\. ]+)<([[:alnum:]&\-+=%,\.]+)@([[:alnum:]-.]+)>\[/email\]#si'
    				=> '<div class="linktag">'.$eml_tag.'<a href="mailto://\\2@\\3" class="lt-url">\\1 &lt;\\2@\\3&gt;</a></div>',

    // Email @: link, without extra text: [email]local@addr[/email]
    '#\[email\]([[:alnum:]&\-+=%,\.]+)@([[:alnum:]\-\.]+)\[/email\]#si'
    				=> '<div class="linktag">'.$eml_tag.'<a href="mailto://\\1@\\2" class="lt-url">\\1@\\2</a></div>',

    // Email @: link, with extra text: [email=local@addr]name[/email]
    '#\[email=([[:alnum:]&\-+=%,\.]+)@([[:alnum:]\-\.]+)\](.*?)\[/email\]#si'
     				=> '<div class="linktag">'.$eml_tag.'<a href="mailto://\\1@\\2" class="lt-url">\\3</a></div>',
  

    // Telephone t: link, with extra text: [telno=link]text[/telno]
    '#\[telno\]([[:digit:]- ]+)\[/telno\]#si'
    				=> '<div class="linktag">'.$tel_tag.'\\1</div>',

    // Telephone t: link, with extra text: [telno=link]text[/telno] = text is appended!
    '#\[telno=([[:digit:]- ]+)\](.*?)\[/telno\]#si'
    				=> '<div class="linktag">'.$tel_tag.'\\1 (\\2)</div>',


    // Document link, without extra text: [doclink]link[/doclink]
    '#\[doclink\](http[[:alnum:]:;&,%_+~!=@\/\.\-\#\?]+?)\[/doclink\]#si'
    				=> '<div class="linktag">'.$doc_tag.'<a href="\\1" class="lt-url">\\1</a></div>',

    // Document link, with extra text: [doclink=link]text[/doclink]
    '#\[doclink=(http[[:alnum:]:;&,%_+~!=@\/\.\-\#\?]+?)\](.*?)\[/doclink\]#si'
    				=> '<div class="linktag">'.$doc_tag.'<a href="\\1" class="lt-url"'.$title_attr_2.'>\\2</a></div>',

    // Document link, initial '/' for site-relative, without extra text: [doclink]link[/doclink]
    '#\[doclink\](/[[:alnum:]:;&,%_+~!=@\/\.\-\#\?]+?)\[/doclink\]#si'
    				=> '<div class="linktag">'.$doc_tag.'<a href="\\1" class="lt-url">\\1</a></div>',

    // Document link, initial '/' for site-relative, with extra text: [doclink=link]text[/doclink]
    '#\[doclink=(/[[:alnum:]:;&,%_+~!=@\/\.\-\#\?]+?)\](.*?)\[/doclink\]#si'
    				=> '<div class="linktag">'.$doc_tag.'<a href="\\1" class="lt-url"'.$title_attr_2.'>\\2</a></div>',

    // Document link, without initial http:// prefix or extra text: [doclink]link[/doclink]
    '#\[doclink\]([[:alnum:]:;&,%_+~!=@\/\.\-\#\?]+?)\[/doclink\]#si'
    				=> '<div class="linktag">'.$doc_tag.'<a href="http://\\1" class="lt-url">\\1</a></div>',

    // Document link, without initial http:// prefix but with extra text: [doclink=link]text[/doclink]
    '#\[doclink=([[:alnum:]:;&,%_+~!=@\/\.\-\#\?]+?)\](.*?)\[/doclink\]#si'
    				=> '<div class="linktag">'.$doc_tag.'<a href="http://\\1" class="lt-url"'.$title_attr_2.'>\\2</a></div>',
    				

    // Internal link, without extra text: [intlink]link[/intlink]
    '#\[intlink\](http[[:alnum:]:;&,%_+~!=@\/\.\-\#\?]+?)\[/intlink\]#si'
    				=> '<div class="linktag">'.$int_tag.'<a href="\\1" class="lt-url">\\1</a></div>',

    // Internal link, with extra text: [intlink=link]text[/intlink]
    '#\[intlink=(http[[:alnum:]:;&,%_+~!=@\/\.\-\#\?]+?)\](.*?)\[/intlink\]#si'
    				=> '<div class="linktag">'.$int_tag.'<a href="\\1" class="lt-url"'.$title_attr_2.'>\\2</a></div>',

    // Internal link, initial '/' for site-relative, without extra text: [intlink]link[/intlink]
    '#\[intlink\](/[[:alnum:]:;&,%_+~!=@\/\.\-\#\?]+?)\[/intlink\]#si'
    				=> '<div class="linktag">'.$int_tag.'<a href="\\1" class="lt-url">\\1</a></div>',

    // Internal link, initial '/' for site-relative, with extra text: [intlink=link]text[/intlink]
    '#\[intlink=(/[[:alnum:]:;&,%_+~!=@\/\.\-\#\?]+?)\](.*?)\[/intlink\]#si'
    				=> '<div class="linktag">'.$int_tag.'<a href="\\1" class="lt-url"'.$title_attr_2.'>\\2</a></div>',

    // Internal link, without extra text: [intlink]link[/intlink]
    '#\[intlink\]([[:alnum:]:;&,%_+~!=@\/\.\-\#\?]+?)\[/intlink\]#si'
    				=> '<div class="linktag">'.$int_tag.'<a href="http://\\1" class="lt-url">\\1</a></div>',

    // Internal link, with extra text: [intlink=link]text[/intlink]
    '#\[intlink=([[:alnum:]:;&,%_+~!=@\/\.\-\#\?]+?)\](.*?)\[/intlink\]#si'
    				=> '<div class="linktag">'.$int_tag.'<a href="http://\\1" class="lt-url"'.$title_attr_2.'>\\2</a></div>',

  
    // External link, without extra text: [extlink]link[/extlink]
    '#\[extlink\](http[[:alnum:]:;&,%_+~!=@\/\.\-\#\?]+?)\[/extlink\]#si'
    				=> '<div class="linktag">'.$ext_tag.'<a href="\\1" class="lt-url">\\1</a></div>',

    // External link, with extra text: [extlink=link]text[/extlink]
    '#\[extlink=(http[[:alnum:]:;&,%_+~!=@\/\.\-\#\?]+?)\](.*?)\[/extlink\]#si'
    				=> '<div class="linktag">'.$ext_tag.'<a href="\\1" class="lt-url"'.$title_attr_2.'>\\2</a></div>',

    // External link, initial '/' for site-relative, without extra text: [extlink]link[/extlink]
    '#\[extlink\](/[[:alnum:]:;&,%_+~!=@\/\.\-\#\?]+?)\[/extlink\]#si'
    				=> '<div class="linktag">'.$ext_tag.'<a href="\\1" class="lt-url">\\1</a></div>',

    // External link, initial '/' for site-relative, with extra text: [extlink=link]text[/extlink]
    '#\[extlink=(/[[:alnum:]:;&,%_+~!=@\/\.\-\#\?]+?)\](.*?)\[/extlink\]#si'
    				=> '<div class="linktag">'.$ext_tag.'<a href="\\1" class="lt-url"'.$title_attr_2.'>\\2</a></div>',

    // External link, without extra text: [extlink]link[/extlink]
    '#\[extlink\]([[:alnum:]:;&,%_+~!=@\/\.\-\#\?]+?)\[/extlink\]#si'
    				=> '<div class="linktag">'.$ext_tag.'<a href="http://\\1" class="lt-url">\\1</a></div>',

    // External link, with extra text: [extlink=link]text[/extlink]
    '#\[extlink=([[:alnum:]:;&,%_+~!=@\/\.\-\#\?]+?)\](.*?)\[/extlink\]#si'
    				=> '<div class="linktag">'.$ext_tag.'<a href="http://\\1" class="lt-url"'.$title_attr_2.'>\\2</a></div>',
  

    // E-Form (link to webform) link, without extra text: [eform]link[/eform]
    '#\[eform\](http[[:alnum:]:;&,%_+~!=@\/\.\-\#\?]+?)\[/eform\]#si'
    				=> '<div class="linktag">'.$efm_tag.'<a href="\\1" class="lt-url">\\1</a></div>',

    // E-Form (link to webform) link, with extra text: [eform=link]text[/eform]
    '#\[eform=(http[[:alnum:]:;&,%_+~!=@\/\.\-\#\?]+?)\](.*?)\[/eform\]#si'
    				=> '<div class="linktag">'.$efm_tag.'<a href="\\1" class="lt-url"'.$title_attr_2.'>\\2</a></div>',
    				
    // E-Form (link to webform) link, initial '/' for site-relative, without extra text: [eform]link[/eform]
    '#\[eform\](/[[:alnum:]:;&,%_+~!=@\/\.\-\#\?]+?)\[/eform\]#si'
    				=> '<div class="linktag">'.$efm_tag.'<a href="\\1" class="lt-url">\\1</a></div>',

    // E-Form (link to webform) link, initial '/' for site-relative, with extra text: [eform=link]text[/eform]
    '#\[eform=(/[[:alnum:]:;&,%_+~!=@\/\.\-\#\?]+?)\](.*?)\[/eform\]#si'
    				=> '<div class="linktag">'.$efm_tag.'<a href="\\1" class="lt-url"'.$title_attr_2.'>\\2</a></div>',


    // E-Form (link to webform) link, without extra text: [eform]link[/eform]
    '#\[eform\]([[:alnum:]:;&,%_+~!=@\/\.\-\#\?]+?)\[/eform\]#si'
    				=> '<div class="linktag">'.$efm_tag.'<a href="http://\\1" class="lt-url">\\1</a></div>',

    // E-Form (link to webform) link, with extra text: [eform=link]text[/eform]
    '#\[eform=([[:alnum:]:;&,%_+~!=@\/\.\-\#\?]+?)\](.*?)\[/eform\]#si'
    				=> '<div class="linktag">'.$efm_tag.'<a href="http://\\1" class="lt-url"'.$title_attr_2.'>\\2</a></div>',
  );
  $body = preg_replace(array_keys($preg), array_values($preg), $body);

  return $body;
}

