Drupal linktags.module README.txt
==============================================================================

The Drupal linktags.module adds a Linktags filter to Drupal. This allows you
to use HTML-like tags as an alternative to HTML itself for adding markup
to your posts. Linktags is easier to use than HTML and helps to prevent
malicious users from disrupting your site's formatting.

See the help screen of the module (or the code) for information on which
tags and variants are supported. This implementation is not necessarily the
same as the original Linktags implementaion.
 
Note that this filter also recognizes and converts URLs and email addresses
to links automatically.

Installation
------------------------------------------------------------------------------
 
  - Download the Linktags module from http://drupal.org/project/linktags

  - Create a .../modules/linktags/ subdirectory and copy the files into it.

  - Enable the module as usual from Drupal's admin pages 
    (Administer » Modules)
 
Configuration
------------------------------------------------------------------------------

  - Before using Linktags you need to enable the Linktags filter in an input
    format (see Administer » Input formats » add input format)

  - You can enable/ disable the following features in the configuration page
    of the input format in which Linktags is enabled:

    * Convert tags to html links
    * Print debugging info

  - If you've enabled multiple filters, you may need to rearrange them to
    ensure they execute in the correct order. For example, if HTML filtering 
    is enabled, it is essential that Linktags be sorted AFTER the HTML filter. 
    If not this module will change the Linktags into HTML, and the HTML filter 
    will disallow and remove the code again.

Complementing Modules
------------------------------------------------------------------------------

The following optional modules may be used to enhance your Linktags 
installation:

  - BBCode module - http://drupal.org/project/bbcode

Note: This is an independent project. Please do not report issues as Linktags
problems!

Credits / Contacts
------------------------------------------------------------------------------

  - The author of this module is Ruth Ivimey-Cook

  - The code is based largely on the BBCode module, for which many thanks
    to the BBCode projects developers.

TODO List
------------------------------------------------------------------------------

 - Translate this module into other languages.

