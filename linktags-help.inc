<?php

/**
 * @file
 * Instructions on how to use the Linktags implementation.
 */

function _linktags_filter_tip() {
  return t('<a id="filter-linktags"></a><h2>Linktags Guide</h2>

<p>
 Linktags allows you to create the stylised e:, t:, @: links using simple
 tag rules within the text. The tags use a similar style to Linktags, but do
 not require the Linktags module to work.
</p>

<table width="98%">
 <tr>
  <th>usage</th><th>display</th>
 </tr>
 <tr>
  <td>We use [url=http://example.com/]the example site[/url] in these examples</td>
  <td>We use <a href="http://example.com/" target="_blank">the example site</a> in these examples</td>
 </tr>
 <tr>
  <td>We use [url]http://example.com/[/url] in these examples</td>
  <td>We use <a href="http://example.com/" target="_blank">http://example.com/</a> in these examples</td>
 </tr>
</table>

<p>
 Make sure that you take care of the proper order of the opening and closing
 tags. You should close the tags in the opposite order in which you opened
 them. Otherwise you might get very strange rendering results. Also check
 your post with the preview function before submitting it, in case there are
 formatting errors due to improper Linktags usage.
</p>');

}
